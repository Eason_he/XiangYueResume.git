import { Component, OnInit } from '@angular/core';
import {UserBaseInfo} from '../../shared/model/user-base-info';

@Component({
  selector: 'app-base-info',
  templateUrl: './base-info.component.html',
  styleUrls: ['./base-info.component.scss']
})
export class BaseInfoComponent implements OnInit {

  userBaseInfo: UserBaseInfo;

  constructor() { }

  ngOnInit() {
  }

}
