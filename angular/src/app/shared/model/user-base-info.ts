export class UserBaseInfo {
  name: string;
  dateOfBirth: string;
  age: number;
  mobile: number;
  email: string;
  education: string;
  speciality: string;
}
