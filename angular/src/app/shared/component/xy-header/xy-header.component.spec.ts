import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XyHeaderComponent } from './xy-header.component';

describe('XyHeaderComponent', () => {
  let component: XyHeaderComponent;
  let fixture: ComponentFixture<XyHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XyHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XyHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
