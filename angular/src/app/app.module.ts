import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseInfoComponent } from './user-input/base-info/base-info.component';
import { FormInputComponent } from './shared/component/form-input/form-input.component';
import { FormHeaderComponent } from './shared/component/form-header/form-header.component';
import { XyHeaderComponent } from './shared/component/xy-header/xy-header.component';
import { XyFooterComponent } from './shared/component/xy-footer/xy-footer.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseInfoComponent,
    FormInputComponent,
    FormHeaderComponent,
    XyHeaderComponent,
    XyFooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
