/**
 * Copyright Reserved by Eason Y L He
 */
package com.xiangyue.resume.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {
	
	@GetMapping("/hc")
	public String healthCheck() {
		return "xiangyue_resume_ms is running well!";
	}

}
