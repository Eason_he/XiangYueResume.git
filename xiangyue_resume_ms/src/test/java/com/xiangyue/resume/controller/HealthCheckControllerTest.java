/** 
* @CopyRight Reserved by Eason Y L He
* @author Eason Y L He
* @version 2019年8月22日
*/ 

package com.xiangyue.resume.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HealthCheckControllerTest {
	
	private MockMvc mockMvc;
	
	@Autowired  
	private WebApplicationContext context;
	 
	@Before   
	public void setup() {  
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();  
	} 

	@Test
	public void testHealthCheck() throws Exception {
		mockMvc.perform(get("/hc"))
		.andExpect(status().isOk())
		.andExpect(content().string(Matchers.containsString("xiangyue_resume_ms is running well!")));
			
	}

}

